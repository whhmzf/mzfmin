# mzfmin

#### 介绍
微信小程序接口api封装


#### 使用说明
该文件必须赋予读写权限

微信小程序开发封装的api
1. token获取
2. 小程序二维码生成
3. 获取用户信息
4. 订阅消息的发布

配置信息  
Config::loadConfig([
"appid" => "",
"secret" => "",
"autoFlushToken" => true
]);  
获取用户openid  
$res = \mzf\EasyMin\Token\token::getToken();


