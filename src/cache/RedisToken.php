<?php

namespace mzf\EasyMin\cache;

use mzf\EasyMin\Config;
use mzf\EasyMin\exception\MyException;
use mzf\EasyMin\tools\Redis;

 class RedisToken implements BaseCache
{
    //缓存token的key
    protected $tokenKey = "mzf_easymin_token";

    /**
     * 清除accessToken
     * @return int|\Redis
     * @throws MyException
     * @throws \RedisException
     */
    public function clear(){
        $redis = $this->getRedisInstall();
        return $redis->del($this->tokenKey);
    }

    /**
     * 存储accessToken
     * @param $accessToken
     * @param $expireTime
     * @return void
     * @throws MyException
     * @throws \RedisException
     */
    public function saveAccessToken($accessToken,$expireTime){
        $redis = $this->getRedisInstall();
        try {
            $this->clear();
            $res = $redis->set($this->tokenKey, $accessToken, $expireTime);
            if (!$res){
                throw new MyException("redis save accessToken fail",999);
            }
        } catch (\RedisException $e) {
            throw new $e;
        }
    }

    /**
     * 获取redis的实例
     * @return \Redis|null
     * @throws MyException
     */
    protected function getRedisInstall(){
        $CacheConfig = Config::getCacheInfo();
        $redisConfig = $CacheConfig["redis"];
        $redis = Redis::getInstall($redisConfig);
        return $redis;
    }

    /**
     * 获取存储的accessToken
     * @return false|mixed|\Redis|string
     * @throws MyException
     * @throws \RedisException
     */
    public function getAccessToken(){
        $redis = $this->getRedisInstall();
        return $redis->get($this->tokenKey);
    }

}