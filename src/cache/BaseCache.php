<?php

namespace mzf\EasyMin\cache;

interface BaseCache
{
    /**
     * 存储accessToken
     * @param string $accessToken
     * @param int $expireTime 过期时间
     * @return mixed
     */
    public function saveAccessToken($accessToken,$expireTime);

    /**
     * 清除accessToken
     * @return bool|\Exception
     */
    public function clear() ;

    /**
     * 获取accessToken
     * @return mixed
     */
    public function getAccessToken();

}