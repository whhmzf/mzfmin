<?php

namespace mzf\EasyMin\cache;

use mzf\EasyMin\Config;
use mzf\EasyMin\exception\MyException;

class FileToken implements BaseCache
{

    /**
     * @inheritDoc
     */
    public function saveAccessToken($accessToken, $expireTime)
    {
        $saveData = [
            "access_token"=>$accessToken,
            "expire_in"=>$expireTime,
            "create_time"=>time(),
        ];
        $this->createCacheFile($saveData);
    }

    protected function createCacheFile($saveData){
        $this->clear();
        $fileName = $this->getCacheFileName();
        $filePath = Config::getAccessTokenACachePath();
        if (!file_exists($filePath)) {
            $res = @mkdir($filePath, 0777, true);
            if (!$res){
                throw new MyException("mzf/easymin No authority to create files",999);
            }
        }
        $filePath .= $fileName;
        $this->checkFile($filePath,true);
        $logs_data = serialize($saveData);
        return file_put_contents($filePath, $logs_data, FILE_APPEND);
    }

    /**
     * 检测删除文件
     * @param $filePath
     * @param $isClear
     * @return void
     * @throws MyException
     */
    protected function checkFile($filePath,$isClear){
        if (!is_file($filePath)){
            return;
        }
        if ($isClear){
            $res = unlink($filePath);
            if (!$res){
                throw new MyException("fileCache unlink fail");
            }
        }
    }

    /**
     * 获取缓存的文件名
     * @return string
     */
    protected function getCacheFileName(){
       return "wechat_min_access_token_".Config::getAppid();
    }

    protected function getSaveFilePath(){
        $fileName = $this->getCacheFileName();
        $filePath = Config::getAccessTokenACachePath();
        return $filePath.$fileName;
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $filePath = $this->getSaveFilePath();
        $this->checkFile($filePath,true);
    }

    /**
     * @inheritDoc
     */
    public function getAccessToken()
    {
        $filePath = $this->getSaveFilePath();
        if (!is_file($filePath)){
            return null;
        }
        $content = file_get_contents($filePath);
        if (!$content){
            return null;
        }
        $saveData =  unserialize($content);
        if ($saveData["create_time"] + $saveData["expire_in"] <=  time()){
            return null;
        }
        return $saveData["access_token"];
    }
}