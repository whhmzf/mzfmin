<?php

namespace mzf\EasyMin\exception;

class MyException extends \Exception
{
    protected $errcode= null;

   public function __construct($message = "",$errCode=0, $code = 0, \Throwable $previous = null)
   {
       $this->errcode = $errCode;
       parent::__construct($message, $code, $previous);
   }



}