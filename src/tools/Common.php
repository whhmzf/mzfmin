<?php

namespace mzf\EasyMin\tools;

use DateTime;
use mzf\EasyMin\Config;

class Common
{

    /**
     * 时间转时间戳
     * @param string $startTime 开始时间
     * @param string $endTime 结束时间
     * @param boolean $isYear 是否只是年月日
     * @return array
     * [
     *  "startTime"=>$startTime, //开始时间
         "endTime"=>$endTime  //结束时间
     * ]
     */
    static  function dateToStrtotime($startTime,$endTime,$isYear = false ){
        if ($isYear){
            $startTime .= " 00:00:00";
            $startTime = strtotime($startTime);
            $endTime .= " 23:59:59";
            $endTime = strtotime($endTime);
        }else {
            $startTime = strtotime($startTime);
            $endTime = strtotime($endTime);
        }
        return [
          "startTime"=>$startTime,
          "endTime"=>$endTime
        ];
    }

    /**
     * 通过身份证获取年龄、性别、出生年月
     * @param $idCard
     * @return array [
     *      'age'=>12, //年龄
     *      'sex'=>'M',//性别 M：男,F:女
     *      'birthday'=>'2023-01-12',//生日
     *  ]
     */
   static function getAgeAndSex($idCard)
    {
        if (!self::validateChineseIDCard($idCard)){
            throw new \Exception("身份证不合法");
        }
        // 获取生日日期
        $birthday = substr($idCard, 6, 8);
        $year = substr($birthday, 0, 4);
        $month = substr($birthday, 4, 2);
        $day = substr($birthday, 6, 2);
        // 计算年龄
        $age = date('Y') - $year;
        if (date('m') < $month || (date('m') == $month && date('d') < $day)) {
            $age--;
        }
        // 获取性别
        $genderCode = substr($idCard, 16, 1);
        $gender = $genderCode % 2 === 0 ? '女' : '男';

        // 获取生日
        $birthdayDate = DateTime::createFromFormat('Ymd', $birthday);
        $birthdayStr = $birthdayDate->format('Y-m-d');
        return [
            "age" => $age,
            "sex" => $gender == "女" ? "M" : "F",
            "birthday" => $birthdayStr
        ];
    }

    /**
     * 验证身份证是否合法
     * @param $idCard
     * @return bool
     */
    static function validateChineseIDCard($idCard)
    {
        // 正则表达式匹配身份证号码格式
        $pattern = '/^\d{17}[\dXx]$/';

        if (preg_match($pattern, $idCard)) {
            return true; // 符合身份证号码格式
        } else {
            return false; // 不符合身份证号码格式
        }
    }

    /**
     * 检测是否请求成功，不适用于图片的结果验证
     * @param $responseData
     * @return bool
     */
    static function isSuccess($responseData){
        if (isset($responseData["errcode"]) && $responseData["errcode"] != 0) {
            return false;
        } else {
            return true;
        }
    }

    //日志记录
    static function logs($msg, $type = 'order')
    {
        $logs_filename = Config::getLogPath();
        if (is_array($msg)){
            $msg = json_encode($msg,JSON_UNESCAPED_UNICODE);
        }
        if (!file_exists($logs_filename)) {
            @mkdir($logs_filename, 0777, true);
        }
        $logs_filename .= date('m-d') . "_" . $type . '.log';
        $logs_data = date('[Y-m-d H:i:s]') . microtime() . ": {$msg}\n";
        return file_put_contents($logs_filename, $logs_data, FILE_APPEND);
    }


    /**
     * 获取指定长度的随机的字符串
     * @param int $length 长度 默认是16
     * @return string
     */
    static function createNonceStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * post请求
     * @param string $url 请求地址
     * @param array $data 参数
     * @return array|mixed $result
     */
    public static function curlPost($url, $post_data, $header = [], $proxy = [], $isJson = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if (!empty($proxy)) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            curl_setopt($ch, CURLOPT_PROXY, "{$proxy['ip']}:{$proxy['port']}");
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, "{$proxy['username']}:{$proxy['password']}");
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);
        curl_close($ch);
        if ($isJson) {
            $result = json_decode($result, true);
        }
        return $result;
    }


    /**
     * get请求
     * @param $url
     * @param $options
     * @return bool|string
     */
    static function curlGet($url = '', $options = array(), $isJson = true)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($isJson){
            $data = json_decode($data, true);
        }
        return $data;
    }


}