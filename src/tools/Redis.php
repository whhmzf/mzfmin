<?php

namespace mzf\EasyMin\tools;

use mzf\EasyMin\exception\MyException;

class Redis
{
    protected static $host = "127.0.0.1";

    protected static $port = "6379";


    protected static $timeout = 0;

    protected static $password = "";

    protected static $db = 0;

    protected static $expire = 0;

    public static $install = null;

    private function __construct()
    {

    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    public static function getInstall($redisConfig)
    {
        if (self::$install == null) {
            if (!extension_loaded('redis')) {
                throw new MyException("no support redis ", 999);
            }
            if (isset($redisConfig["host"])) {
                self::$host = $redisConfig['host'];
            }
            self::$port = isset($redisConfig["port"]) ? $redisConfig["port"] : "6379";
            self::$password = isset($redisConfig["password"]) ? $redisConfig["password"] : "";
            self::$db = isset($redisConfig["db"]) ? $redisConfig["db"] : 1;
            self::$timeout = isset($redisConfig["timeout"]) ? $redisConfig["timeout"] : 0;
            self::$expire = isset($redisConfig["expire"]) ? $redisConfig['expire'] : 0;
            self::$install = new \Redis();
            try {
                $res = self::$install->connect(self::$host, self::$port, self::$timeout);
                if (!$res) {
                    throw new MyException("redis connect fail", 999);
                }
            } catch (\RedisException $e) {
                throw new MyException($e->getMessage(), 999);
            }
            if (self::$password != "") {
                try {
                    $res = self::$install->auth(self::$password);
                    if (!$res) {
                        throw new MyException("redis auth fail", 999);
                    }
                } catch (\RedisException $e) {
                    throw new MyException($e->getMessage(), 999);
                }
            }
            self::$install->select(self::$db);
        }
        return self::$install;
    }


}