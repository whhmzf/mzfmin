<?php

namespace mzf\EasyMin;

class Config
{
    static protected $minConfig = array();
    //小程序appid
    protected static $appid = "";
    //小程序secret
    protected static $secret = "";

    //token过期了是否自动刷新
    protected static $autoFlushToke = false;

    //日志存放记录
    protected static $logPath = __DIR__ . "/logs/";

    //缓存信息 类型 file | redis
    protected static $cacheInfo = array();

    protected static $accessTokenCachePath = __DIR__ . "/logs/";


    protected static $redisInfo = [
        "host" => "127.0.0.1",
        "port" => "6379",
        "password" => ""
    ];

    /**
     * 加载配置信息
     * @param $config
     * @return void
     * @throws \Exception
     */
    public static function loadConfig($config = array())
    {
        if (!is_array($config)) {
            throw new \Exception("config 类型错误");
        }
        if (isset($config["appid"])) {
            self::$appid = $config["appid"];
        }
        if (isset($config["secret"])) {
            self::$secret = $config["secret"];
        }
        if (isset($config["autoFlushToken"])) {
            self::$autoFlushToke = $config["autoFlushToken"];
        }
        if (isset($config["logPath"])) {
            self::$logPath = $config["logPath"];
        }

        if (isset($config["cacheInfo"]) && is_array($config["cacheInfo"])) {
            $cacheInfo = $config["cacheInfo"];
            self::$cacheInfo["type"] = isset($cacheInfo["type"]) && in_array($cacheInfo["type"], ["file", "redis"]) ? $cacheInfo["type"] : "file";
            if (self::$cacheInfo["type"] == "redis") {
                if (isset($cacheInfo["host"])) {
                    self::$redisInfo["host"] = $cacheInfo["host"];
                }
                if (isset($cacheInfo["port"])) {
                    self::$redisInfo["port"] = $cacheInfo["port"];
                }
                if (isset($cacheInfo["password"])) {
                    self::$redisInfo["password"] = $cacheInfo["password"];
                }
                self::$cacheInfo["redis"] = self::$redisInfo;
            }
        } else {
            self::$cacheInfo["type"] = "file";
        }


    }

    public static function getConfig()
    {
        self::$minConfig = [
            "appid" => self::$appid,
            "secret" => self::$secret,
            "logPath" => self::$logPath,
            "cacheType" => self::$cacheInfo
        ];
        return self::$minConfig;
    }

    public static function autoFlushToke()
    {
        return self::$autoFlushToke;
    }

    /**
     * 获取appid
     * @return string
     */
    public static function getAppid()
    {
        return self::$appid;
    }

    /**
     * 获取secret
     * @return string
     */
    public static function getSecret()
    {
        return self::$secret;
    }

    /**
     * 获取日志路径
     * @return string
     */
    public static function getLogPath()
    {
        return self::$logPath;
    }

    /**
     * 获取缓存的配置信息
     * @return array
     */
    public static function getCacheInfo()
    {
        return self::$cacheInfo;
    }

    public static function getAccessTokenACachePath()
    {
        return self::$accessTokenCachePath;
    }


}