<?php

namespace mzf\EasyMin\commonApi;

use Exception;
use mzf\EasyMin\Token\token;
use mzf\EasyMin\tools\Common;

class QrCode extends Base
{
    const limitMinQrCodeUrl = "wxa/getwxacode?access_token=%s";

    const unlimitedQRCodeUrl = "wxa/getwxacodeunlimit?access_token=%s";

    const createWxaQrCodeUrl = "cgi-bin/wxaapp/createwxaqrcode?access_token=%s";

    /**
     * 获取小程序二维码，适用于需要的码数量较少的业务场景。通过该接口生成的小程序码，永久有效，有数量限制  https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/qr-code/createQRCode.html
     * @param array $data 小程序文档传参相同
     * @param string $savePath 不保存不需要填写 会返回图片的二进制信息
     * @return array|mixed|string[]
     * @throws Exception
     */
    public static function getCreateWxaQrCode($data, $savePath = "")
    {
        $url = sprintf(self::urlHost . self::createWxaQrCodeUrl, token::accessToken());
        return self::checkCommon($url, $data, $savePath);
    }


    /**
     * 获取不受限致的小程序码 https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/qr-code/getUnlimitedQRCode.html
     * @param array $data 小程序文档传参相同
     * @param string $savePath 不保存不需要填写 会返回图片的二进制信息
     * @return array|mixed|string[]
     * @throws Exception
     */
    public static function getUnlimitedQRCode($data, $savePath = "")
    {
        if ((isset($data["scene"]) && $data["scene"] == "") || !isset($data["scene"])) {
            $data["scene"] = "version=*";
        }
        $url = sprintf(self::urlHost . self::unlimitedQRCodeUrl, token::accessToken());

        return self::checkCommon($url, $data, $savePath);
    }

    /**
     * 返回数据的检测和封装
     * @param string $url 请求地址
     * @param array $data 请求参数
     [
        "page" => "",
        "scene" => "",
        "env_version" => "",
        "check_path" => false
     ]
     * @param string $savePath 图片保存的路径
     * @return array|mixed|string[]
     * @throws Exception
     */
    protected static function checkCommon($url, $data, $savePath)
    {
        $res = self::apiRequest($url, $data, "post",false);
        $resJson = json_decode($res,true);
        if ($resJson){
            if (isset($resJson["errcode"])) {
                return $resJson;
            }
        }
        if ($savePath != "") {
            return self::savePic($savePath, $res);
        }
        return $res;

    }


    /**
     * 获取小程序码，有次数限制的   https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/qr-code/getQRCode.html
     * @param array $data 小程序文档传参相同
     * @param string $savePath 图片保存路径 不保存不需要填写 会返回图片的二进制信息
     * @return array|mixed
     */
    public static function getLimitMinQrCode($data, $savePath = "")
    {
        $url = sprintf(self::urlHost . self::limitMinQrCodeUrl, token::accessToken());
        return self::checkCommon($url, $data, $savePath);
    }

    /**
     * 保存图片
     * @param string $path 保存的路径
     * @param mixed $info 图片的二进制
     * @return array|string[]
     * @throws Exception
     */
    protected static function savePic($path, $info)
    {
        if (!file_exists($path)) {
            $res = @mkdir($path, 0777, true);
            if (!$res) {
                throw new Exception("{$path} No authority to create files", 999);
            }
        }
        $saveName = Common::createNonceStr(20);
        $res = file_put_contents($path . "/{$saveName}.png", $info);
        if (!$res) {
            return [
                "errcode" => 999,
                "msg" => "pic save fail",
                "zh_msg" => "图片保存失败"
            ];
        }
        return [
            "url" => $path . "/{$saveName}.png"
        ];

    }

}