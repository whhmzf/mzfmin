<?php

namespace mzf\EasyMin\commonApi;

use mzf\EasyMin\Config;

class Login extends Base
{
    protected static $url = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
    protected static $logKey = "login_error";

    /**
     * 登录
     * @param string $code 小程序端 wx.login 获取的code  https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-login/code2Session.html
     * @return bool|string [["session_key"]=>"","openid"=>""]
     */
    public static function begin($code){
        $loginUrl = sprintf(self::$url,Config::getAppid(),Config::getSecret(),$code);
        $info = self::apiRequest($loginUrl, []);
        return self::sendSuccess($info);
    }


}