<?php

namespace mzf\EasyMin\commonApi;

use mzf\EasyMin\Token\token;
use mzf\EasyMin\tools\Common;

class User extends Base
{
    /**
     * 获取用户手机号 https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-info/phone-number/getPhoneNumber.html
     * @param $code
     * @return array|bool
     */
    public static function getPhone($code)
    {
        $url = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s";
        $url = sprintf($url, token::accessToken());
        $data = [
            "code" => $code
        ];
        $info = self::apiRequest($url, $data, "post");
        return self::sendSuccess($info);
    }

}