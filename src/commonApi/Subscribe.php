<?php

namespace mzf\EasyMin\commonApi;

use mzf\EasyMin\exception\MyException;
use mzf\EasyMin\Token\token;

/**
 * 订阅消息
 */
class Subscribe extends Base
{

    const getCategoryUrl = self::urlHost . "wxaapi/newtmpl/getcategory?access_token=%s";

    const getMessageTemplateListUrl = self::urlHost . "wxaapi/newtmpl/gettemplate?access_token=%s";

    const sendMessageTemplateUrl = self::urlHost . "cgi-bin/message/subscribe/send?access_token=%s";


    /**
     * 发送订阅消息
     * @param string $templateId 模板id
     * @param array $data 模板内容
     * @param string $touser 接受者的openid
     * @param string $page 跳转的页面
     * @param string $miniprogram_state 跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
     * @return mixed|null
     * @throws MyException
     */
    public static function send($templateId, $data, $touser, $page = "", $miniprogram_state = "formal")
    {
        $url = self::getRequestUlr(self::sendMessageTemplateUrl);
        if ($templateId == "") {
            throw new MyException("模板id不能为空", 8686);
        }
        if (count($data) <= 0) {
            throw new MyException("发送的数据data不能为空", 8686);
        }
        if ($touser == "") {
            throw new MyException("发送人的openid不能为空", 8686);
        }
        $sendData = [
            "touser" => $touser,
            "template_id" => $templateId,
            "data" => $data,
            "miniprogram_state" => $miniprogram_state,
        ];
        if ($page != "") {
            $sendData["page"] = $page;
        }
        $info = self::apiRequest($url, $sendData, "post");
        return self::sendSuccess($info);

    }

    /**
     * 该接口用于获取当前账号下的个人模板列表。
     * @return mixed|null
     */
    public static function getMessageTemplate()
    {
        $url = self::getRequestUlr(self::getMessageTemplateListUrl);
        $info = self::apiRequest($url);
        return self::sendSuccess($info);
    }

    /**
     * 获取小程序账号的类目  https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/mp-message-management/subscribe-message/getCategory.html
     * @return mixed|null
     */
    public static function getCategory()
    {
        $url = self::getRequestUlr(self::getCategoryUrl);
        $info = self::apiRequest($url);
        return self::sendSuccess($info);
    }


}