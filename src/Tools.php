<?php

namespace mzf\EasyMin;

class Tools
{

    //日志记录
    static function logs($msg, $type = 'order')
    {
        $logs_filename = Config::getLogPath();
        if (is_array($msg)){
            $msg = json_encode($msg,JSON_UNESCAPED_UNICODE);
        }
        if (!file_exists($logs_filename)) {
            @mkdir($logs_filename, 0777, true);
        }
        $logs_filename .= date('m-d') . "_" . $type . '.log';
        $logs_data = date('[Y-m-d H:i:s]') . microtime() . ": {$msg}\n";
        return file_put_contents($logs_filename, $logs_data, FILE_APPEND);
    }


    function createNonceStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * post请求
     * @param string $url 请求地址
     * @param array $data 参数
     * @return array $result
     */
    public static function curlPost($url, $post_data, $header = [], $proxy = [], $isJson = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        if (!empty($proxy)) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            curl_setopt($ch, CURLOPT_PROXY, "{$proxy['ip']}:{$proxy['port']}");
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, "{$proxy['username']}:{$proxy['password']}");
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($ch);
        curl_close($ch);
        if ($isJson) {
            $result = json_decode($result, true);
        }
        return $result;
    }


    /**
     * get请求
     * @param $url
     * @param $options
     * @return bool|string
     */
    function curlGet($url = '', $options = array(), $timeout = 30)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($data, true);
        return $data;
    }


}