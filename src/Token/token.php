<?php

namespace mzf\EasyMin\Token;

use mzf\EasyMin\cache\FileToken;
use mzf\EasyMin\cache\RedisToken;
use mzf\EasyMin\Config;
use mzf\EasyMin\Tools;
use mzf\EasyMin\tools\Common;

class token
{
    protected static $getTokenUrl = "https://api.weixin.qq.com/cgi-bin/stable_token";
    protected static $logKey = "token_error";

    /**
     * 获取accessToken和isCache  true是从缓存中获取的，false不是
     * @return array
     */
    public static function getToken()
    {
        $cacheInstall = self::getCacheInstall();
        $accessToken = $cacheInstall->getAccessToken();
        if ($accessToken){
            return ["access_token"=>$accessToken,"isCache"=>true];
        }
       return self::getNewAccessToken();
    }

    /**
     * 直接返回access_token值
     * @return mixed|string
     */
    public static function accessToken(){
        $info = self::getToken();
        return isset($info["access_token"]) ? $info["access_token"] : "";
    }

    /**
     * 刷新access_token
     * @return array|mixed
     * @throws \RedisException
     * @throws \mzf\EasyMin\exception\MyException
     */
    public static function flushAccessToken(){
        return self::getNewAccessToken();
    }

    /**
     * 获取新的accessToken
     * @return array|mixed
     * @throws \RedisException
     * @throws \mzf\EasyMin\exception\MyException
     */
    protected static function getNewAccessToken(){
        $res = Common::curlPost(self::$getTokenUrl, [
            "grant_type" => "client_credential",
            "appid" => Config::getAppid(),
            "secret" => Config::getSecret()
        ]);
        if (isset($res["errcode"]) && $res["errcode"] != 0) {
            Common::logs($res, self::$logKey);
            return $res;
        }
        $accessToken = $res["access_token"];
        $expireTime = $res["expires_in"] - 100;
        self::cacheToken($accessToken,$expireTime);
        return  ["access_token"=>$accessToken,"isCache"=>false];
    }

    /**
     * 缓存accessToken
     * @param $accessToken
     * @param $expireTime
     * @return void
     * @throws \RedisException
     * @throws \mzf\EasyMin\exception\MyException
     */
    protected static  function cacheToken($accessToken,$expireTime){
        $cacheType = self::getCacheType();
        if ($cacheType == "file"){
            //文件缓存
            self::fileCache($accessToken,$expireTime);
        }else {
            //redis缓存
            self::redisCache($accessToken,$expireTime);
        }

    }

    protected static function getCacheType(){
        $cacheConfigInfo = Config::getCacheInfo();
        return $cacheConfigInfo["type"] ;
    }

    /**
     * 获取缓存的实例
     * @return FileToken|RedisToken
     */
    protected static function getCacheInstall(){
        $cacheType = self::getCacheType();
        if ($cacheType == "file"){
            return (new FileToken());
        }
        return (new RedisToken());
    }


    protected static function fileCache($accessToken,$expireTime){
        (new FileToken())->saveAccessToken($accessToken,$expireTime);
    }

    /**
     * redis存储accessToken
     * @param $accessToken
     * @param $expireTime
     * @return void
     * @throws \RedisException
     * @throws \mzf\EasyMin\exception\MyException
     */
    protected static function redisCache($accessToken,$expireTime){
        (new RedisToken())->saveAccessToken($accessToken,$expireTime);
    }

}